package util;

public class StringUtil {

	public static String upperCaseFirstChar(String string) {

		return string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();

	}

}
