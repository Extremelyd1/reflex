package util;

import org.bukkit.EntityEffect;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

import reflex.Reflex;

public class FireworkUtil {

	/**
	 * Play a pretty firework at the location with the FireworkEffect when
	 * called
	 * 
	 * @param world
	 * @param loc
	 * @param fe
	 */
	public static void playFirework(World world, Location loc, FireworkEffect fe) {
		final Firework fw = world.spawn(loc, Firework.class);
		FireworkMeta meta = fw.getFireworkMeta();
		meta.addEffect(fe);
		meta.setPower(0);
		fw.setFireworkMeta(meta);
		new BukkitRunnable() {
			@Override
			public void run() {
				fw.playEffect(EntityEffect.FIREWORK_EXPLODE);
				fw.remove();
			}
		}.runTaskLater(Reflex.instance, 2L);
	}

}
