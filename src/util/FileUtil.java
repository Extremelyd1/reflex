package util;

import java.io.File;

import org.bukkit.World;

public class FileUtil {

	public static void purgeWorldFiles(World world) {

		for (File file : world.getWorldFolder().listFiles()) {

			if (!file.getName().equals("data") && !file.getName().equals("level.dat")
					&& !file.getName().equals("region")) {

				if (!file.delete())
					file.deleteOnExit();

			}

		}

	}

}
