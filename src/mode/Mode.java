package mode;

public abstract class Mode {

	public abstract void start();

	public abstract void end();

	public abstract void end(String winner);

	public abstract void reset();

}
