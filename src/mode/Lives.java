package mode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import handler.ItemHandler;
import handler.MessageHandler;
import handler.SpectatorHandler;
import reflex.GameManager;
import reflex.GameState;
import reflex.Reflex;

public class Lives extends Mode {

	public ArrayList<UUID> alive = new ArrayList<>();

	public HashMap<UUID, Integer> lifes = new HashMap<UUID, Integer>();

	public void start() {

		GameManager.scoreboardHandler.constructBoard();

		GameManager.scoreboardHandler.addPlayers();

		MessageHandler.sendStartMessage();

		for (Player p : Bukkit.getOnlinePlayers()) {

			SpectatorHandler.setInteractable(p, true);

			p.teleport(GameManager.getRandomSpawn());

			p.playSound(p.getLocation(), Sound.ENDERDRAGON_HIT, 20, 1);

			p.setArrowsStuck(0);

			alive.add(p.getUniqueId());

			int maxLives = GameManager.maxLives;

			lifes.put(p.getUniqueId(), maxLives);

			GameManager.scoreboardHandler.setPoints(p.getName(), maxLives, true);

			MessageHandler.sendTitle(p, ChatColor.GREEN + "Game has started!",
					ChatColor.YELLOW + "You have " + ChatColor.RED + maxLives + ChatColor.YELLOW + " life"
							+ (maxLives == 1 ? "" : "s") + " remaining!");

		}

		ItemHandler.applyKit();

	}

	public void end() {

		MessageHandler.broadcastEndMessage();

		ItemHandler.clear();

		SpectatorHandler.makeAllVisible();

		SpectatorHandler.setAllInteractable(false);

		reset();

	}

	public void end(String winner) {

		MessageHandler.broadcastEndMessage(winner);

		ItemHandler.clear();

		SpectatorHandler.makeAllVisible();

		SpectatorHandler.setAllInteractable(false);

		reset();

		if (GameManager.auto) {

			GameManager.state = GameState.STOPPING;

			Bukkit.getScheduler().scheduleSyncDelayedTask(Reflex.instance, new Runnable() {

				public void run() {

					for (Player p : Bukkit.getOnlinePlayers())
						p.kickPlayer(ChatColor.GREEN + "Thanks for playing!");

					Bukkit.shutdown();

				}

			}, 200L);

		}

	}

	public void reset() {

		alive.clear();

		lifes.clear();

	}

}
