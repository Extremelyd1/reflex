package mode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import handler.ItemHandler;
import handler.MessageHandler;
import handler.SpectatorHandler;
import reflex.GameManager;
import reflex.GameState;
import reflex.Reflex;

public class Elimination extends Mode {

	public HashMap<UUID, Integer> scores = new HashMap<>();

	public ArrayList<UUID> alive = new ArrayList<>();

	private int time = 120;
	private int timerTaskId;

	public void start() {

		GameManager.scoreboardHandler.constructBoard();

		GameManager.scoreboardHandler.addPlayers();

		MessageHandler.sendStartMessage();

		for (Player p : Bukkit.getOnlinePlayers()) {

			SpectatorHandler.setInteractable(p, true);

			p.teleport(GameManager.getRandomSpawn());

			p.playSound(p.getLocation(), Sound.ENDERDRAGON_HIT, 20, 1);

			p.setArrowsStuck(0);

			alive.add(p.getUniqueId());

			scores.put(p.getUniqueId(), 0);

		}

		ItemHandler.applyKit();

		startTimer();

	}

	public void end() {

		MessageHandler.broadcastEndMessage();

		ItemHandler.clear();

		SpectatorHandler.makeAllVisible();

		SpectatorHandler.setAllInteractable(false);

		reset();

	}

	public void end(String winner) {

		MessageHandler.broadcastEndMessage(winner);

		ItemHandler.clear();

		SpectatorHandler.makeAllVisible();

		SpectatorHandler.setAllInteractable(false);

		reset();

		if (GameManager.auto) {

			GameManager.state = GameState.STOPPING;

			Bukkit.getScheduler().scheduleSyncDelayedTask(Reflex.instance, new Runnable() {

				public void run() {

					for (Player p : Bukkit.getOnlinePlayers())
						p.kickPlayer(ChatColor.GREEN + "Thanks for playing!");

					Bukkit.shutdown();

				}

			}, 200L);

		}

	}

	public void eliminate() {

		Player p = getLowestScore();

		MessageHandler.sendEliminateMessage(p);
		
		alive.remove(p.getUniqueId());

		p.teleport(GameManager.getRandomSpawn());

		p.getInventory().clear();

		// Reset scores
		GameManager.scoreboardHandler.removePlayer(p);

		if (alive.size() != 1) {

			for (UUID alivePlayerUUID : alive) {

				GameManager.scoreboardHandler.setPoints(Bukkit.getPlayer(alivePlayerUUID).getName(), 0, false);

			}

			startTimer();

		} else {

			String winnerName = Bukkit.getPlayer(alive.get(0)).getName();

			end(winnerName);

		}

	}

	public Player getLowestScore() {

		Player lowest = null;

		for (Player p : Bukkit.getOnlinePlayers()) {

			if (!alive.contains(p.getUniqueId()))
				continue;

			if (lowest == null) {

				lowest = p;

			} else {

				if (scores.get(lowest.getUniqueId()) == scores.get(p.getUniqueId())) {

					// Select random person with shared lowest score
					if (GameManager.random.nextBoolean())
						lowest = p;

				} else if (scores.get(lowest.getUniqueId()) > scores.get(p.getUniqueId())) {

					lowest = p;

				}

			}
		}

		return lowest;

	}

	public void startTimer() {

		MessageHandler.sendEliminationTimerMessage();
		
		Elimination instance = this;

		timerTaskId = Bukkit.getScheduler().scheduleSyncRepeatingTask(Reflex.instance, new Runnable() {

			public void run() {

				if (!GameManager.state.equals(GameState.IN_GAME))
					Bukkit.getScheduler().cancelTask(timerTaskId);

				if (time == 60 || time == 30) {

					MessageHandler.sendTimeLeftMessage(time);

				}

				String scoreboardTitle = ((int) time / 60) + ":"
						+ (((int) time % 60) < 10 ? "0" + ((int) time % 60) : ((int) time % 60));

				GameManager.scoreboardHandler.updateObjectiveName(scoreboardTitle);

				if (time <= 15) {

					if (time % 2 == 0)
						GameManager.scoreboardHandler.flash(getLowestScore(), instance);

				}

				if (time == 0) {

					Bukkit.getScheduler().cancelTask(timerTaskId);

					eliminate();

				}

				time--;

			}

		}, 0, 20L);

	}

	public void reset() {

		scores.clear();

		alive.clear();

		time = 120;

	}

}
