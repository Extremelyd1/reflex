package mode;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import handler.ItemHandler;
import handler.MessageHandler;
import handler.SpectatorHandler;
import reflex.GameManager;
import reflex.GameState;
import reflex.Reflex;

public class Normal extends Mode {

	public HashMap<UUID, Integer> scores = new HashMap<>();

	public void start() {

		GameManager.scoreboardHandler.constructBoard();

		GameManager.scoreboardHandler.addPlayers();

		MessageHandler.sendStartMessage();

		for (Player p : Bukkit.getOnlinePlayers()) {

			p.teleport(GameManager.getRandomSpawn());

			p.playSound(p.getLocation(), Sound.ENDERDRAGON_HIT, 20, 1);

			p.setArrowsStuck(0);

			scores.put(p.getUniqueId(), 0);

		}

		SpectatorHandler.setAllInteractable(true);

		ItemHandler.applyKit();

	}

	public void end() {

		MessageHandler.broadcastEndMessage();

		ItemHandler.clear();

		SpectatorHandler.setAllInteractable(false);

		reset();

	}

	public void end(String winner) {

		MessageHandler.broadcastEndMessage(winner);

		ItemHandler.clear();

		SpectatorHandler.setAllInteractable(false);

		reset();

		if (GameManager.auto) {

			GameManager.state = GameState.STOPPING;

			Bukkit.getScheduler().scheduleSyncDelayedTask(Reflex.instance, new Runnable() {

				public void run() {

					for (Player p : Bukkit.getOnlinePlayers())
						p.kickPlayer(ChatColor.GREEN + "Thanks for playing!");

					Bukkit.shutdown();

				}

			}, 200L);

		}

	}

	public void reset() {

		scores.clear();

	}

}
