package handler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import mode.Elimination;
import mode.ModeType;
import reflex.Reflex;

public class ScoreboardHandler {

	private Scoreboard board;
	private Objective obj;
	private Score score;
	private Team red;
	private Team green;

	private ModeType type;

	public ScoreboardHandler(ModeType type) {

		this.type = type;

	}

	public void constructBoard() {

		board = Bukkit.getScoreboardManager().getNewScoreboard();

		obj = board.registerNewObjective("Kills", "dummy");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);

		switch (type) {
		case NORMAL:
			obj.setDisplayName(ChatColor.GOLD + "First to " + Bukkit.getOnlinePlayers().size() * 10);
			break;

		case ELIMINATION:
			obj.setDisplayName(ChatColor.GOLD + "    Kills:    ");
			break;

		case LIVES:
			obj.setDisplayName(ChatColor.GOLD + "    Lifes:    ");
			break;
		}

		red = board.registerNewTeam("red");
		green = board.registerNewTeam("green");

		red.setPrefix(ChatColor.DARK_RED + "");
		green.setPrefix(ChatColor.GREEN + "");

	}

	public void updateObjectiveName(String newText) {

		if (type.equals(ModeType.NORMAL)) {

			obj.setDisplayName(ChatColor.GOLD + "First to " + Bukkit.getOnlinePlayers().size() * 10);

		} else if (type.equals(ModeType.ELIMINATION)) {

			obj.setDisplayName(ChatColor.GOLD + newText);

		}

		broadcastBoard();

	}

	public void addPlayer(Player p) {

		score = obj.getScore(p.getName());

		// Change score quickly so it becomes visible on the board
		score.setScore(1);
		score.setScore(0);

		broadcastBoard();

	}

	public void removePlayer(Player p) {

		resetScore(p.getName());

		if (board.getEntryTeam(p.getName()) != null)
			board.getEntryTeam(p.getName()).removeEntry(p.getName());

		broadcastBoard();

	}

	public void addPlayers() {

		for (Player p : Bukkit.getOnlinePlayers()) {

			score = obj.getScore(p.getName());

			// Change score quickly so it becomes visible on the board
			score.setScore(1);
			score.setScore(0);

		}

		broadcastBoard();

	}

	public void setPoints(String name, int amount, boolean add) {

		resetScore(name);

		if (add)
			green.addEntry(name);
		else
			red.addEntry(name);

		obj.getScore(name).setScore(amount);

		broadcastBoard();

		Bukkit.getScheduler().scheduleSyncDelayedTask(Reflex.instance, new Runnable() {

			public void run() {

				if (add)
					green.removeEntry(name);
				else
					red.removeEntry(name);

				resetScore(name);

				obj.getScore(name).setScore(amount);

				broadcastBoard();

			}

		}, 20L);

	}

	// Not very elegant, searching for better solution
	public void flash(Player p, Elimination mode) {

		String name = p.getName();

		resetScore(name);

		red.addEntry(name);

		obj.getScore(name).setScore(mode.scores.get(p.getUniqueId()));

		Bukkit.getScheduler().scheduleSyncDelayedTask(Reflex.instance, new Runnable() {

			public void run() {

				resetScore(name);

				red.removeEntry(name);

				// Check if player is not eliminated yet
				if (obj.getScore(name) != null && mode.scores.containsKey(p.getUniqueId()))
					obj.getScore(name).setScore(mode.scores.get(p.getUniqueId()));

			}

		}, 20L);

	}

	public void resetScore(String name) {

		board.resetScores(name);

		// Just in case the player was flashing or got a kill
		board.resetScores(ChatColor.DARK_RED + name);
		board.resetScores(ChatColor.GREEN + name);

	}

	public void broadcastBoard() {

		for (Player p : Bukkit.getOnlinePlayers())
			p.setScoreboard(board);

	}

	public void clearBoard() {

		board.clearSlot(DisplaySlot.SIDEBAR);

	}

}
