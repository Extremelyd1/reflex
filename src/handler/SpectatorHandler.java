package handler;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class SpectatorHandler {

	public static void setInteractable(Player p, boolean interactable) {

		p.setCollidesWithEntities(interactable);
		p.setAllowFlight(!interactable);

	}

	public static void setAllInteractable(boolean interactable) {

		for (Player p : Bukkit.getOnlinePlayers()) {

			setInteractable(p, interactable);

		}

	}

	public static void makeAllVisible() {

		for (Player p : Bukkit.getOnlinePlayers()) {

			for (Player p2 : Bukkit.getOnlinePlayers()) {

				if (p.equals(p2))
					continue;

				p.showPlayer(p2);

			}

		}

	}

}
