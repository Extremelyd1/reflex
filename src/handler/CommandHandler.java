package handler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mode.ModeType;
import reflex.GameManager;
import reflex.GameState;
import util.StringUtil;

public class CommandHandler {

	public static boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		// Most simple permission system, must be improved!!
		if ((sender instanceof Player) && !((Player) sender).getName().equals("Extremelyd1")) {

			sender.sendMessage(ChatColor.DARK_RED + "You don't have permission!");
			return true;

		}

		if (commandLabel.equalsIgnoreCase("start")) {

			switch (GameManager.state) {

			case PRE_GAME:
			case POST_GAME:
				GameManager.start();
				break;

			case STARTING:
				sender.sendMessage(ChatColor.RED + "Game is already starting!");
				return true;

			case STOPPING:
				sender.sendMessage(ChatColor.RED + "Server is shutting down!");
				return true;

			case IN_GAME:
				sender.sendMessage(ChatColor.RED + "Game is already running!");
				return true;

			default:
				break;

			}

		}

		if (commandLabel.equalsIgnoreCase("end")) {

			switch (GameManager.state) {

			case PRE_GAME:
			case POST_GAME:
				sender.sendMessage(ChatColor.RED + "Game is not running!");
				return true;

			case STOPPING:
				sender.sendMessage(ChatColor.RED + "Server is shutting down!");
				return true;

			case STARTING:
				sender.sendMessage(ChatColor.RED + "Game is starting!");
				return true;

			case IN_GAME:
				GameManager.end();
				break;

			default:
				break;

			}

		}

		if (commandLabel.equalsIgnoreCase("stop")) {

			for (Player p : Bukkit.getOnlinePlayers())
				p.kickPlayer(ChatColor.GREEN + "Thanks for playing!");

			Bukkit.shutdown();

		}

		if (commandLabel.equalsIgnoreCase("auto")) {

			GameManager.auto = !GameManager.auto;

			sender.sendMessage(ChatColor.GREEN + "Auto-mode " + ChatColor.GRAY + "is now "
					+ (GameManager.auto ? ChatColor.GREEN + "on" : ChatColor.RED + "off"));

		}

		if (commandLabel.equalsIgnoreCase("mode")) {

			String modeString = StringUtil.upperCaseFirstChar(GameManager.modeType.toString());

			if (args.length == 0) {

				sender.sendMessage(
						ChatColor.GREEN + modeString + ChatColor.GRAY + " is currently " + ChatColor.GREEN + "active");
				return true;

			}

			switch (GameManager.state) {

			case PRE_GAME:
			case POST_GAME:
			default:
				break;

			case IN_GAME:
				sender.sendMessage(ChatColor.RED + "Game is already running!");
				return true;

			case STARTING:
				sender.sendMessage(ChatColor.RED + "Game is starting!");
				return true;

			case STOPPING:
				sender.sendMessage(ChatColor.RED + "Server is shutting down!");
				return true;

			}

			ModeType newMode = null;

			try {

				newMode = ModeType.valueOf(args[0].toUpperCase());

			} catch (IllegalArgumentException e) {

				sender.sendMessage(ChatColor.RED + "'" + args[0] + "' does not exists");
				return true;

			}

			if (newMode == null) {

				sender.sendMessage(ChatColor.RED + "'" + args[0] + "' does not exists");
				return true;

			} else if (newMode.equals(GameManager.modeType)) {

				sender.sendMessage(ChatColor.RED + "'" + args[0] + "' is already active");
				return true;

			}

			if (GameManager.state.equals(GameState.IN_GAME)) {

				sender.sendMessage(ChatColor.RED + "Can't change gamemode during match");
				return true;

			}

			GameManager.changeMode(newMode);

			Bukkit.broadcastMessage(ChatColor.GREEN + "Gamemode " + ChatColor.GRAY + "changed to " + ChatColor.GREEN
					+ StringUtil.upperCaseFirstChar(newMode.toString()));

		}

		if (commandLabel.equalsIgnoreCase("setMaxLives")) {

			if (args.length == 0) {

				sender.sendMessage(ChatColor.RED + "Usage: /setmaxlives [amount]");
				return true;

			}

			if (!GameManager.modeType.equals(ModeType.LIVES)) {

				sender.sendMessage(ChatColor.RED + "Lives mode not active");
				return true;

			}

			int newMaxLives = Integer.parseInt(args[0]);

			GameManager.maxLives = newMaxLives;

			Bukkit.broadcastMessage(
					ChatColor.GREEN + "Max lives" + ChatColor.GRAY + " has been set to " + ChatColor.RED + newMaxLives);
		}

		if (commandLabel.equalsIgnoreCase("help")) {

			sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_GREEN + "Reflex" + ChatColor.DARK_GRAY + "] "
					+ ChatColor.RED + StringUtil.upperCaseFirstChar(GameManager.modeType.toString()));
			sender.sendMessage(ChatColor.DARK_GRAY + "Start: " + ChatColor.WHITE + "start the match");
			sender.sendMessage(ChatColor.DARK_GRAY + "End: " + ChatColor.WHITE + "force end the match");
			sender.sendMessage(ChatColor.DARK_GRAY + "Auto: " + ChatColor.WHITE + "turn auto-mode on/off");
			sender.sendMessage(ChatColor.DARK_GRAY + "Mode: " + ChatColor.WHITE + "change gamemode");
			sender.sendMessage(ChatColor.DARK_GRAY + "Setmaxlives: " + ChatColor.WHITE + "set the amount of lifes");
			sender.sendMessage(ChatColor.DARK_GRAY + "Stop: " + ChatColor.WHITE + "stop the server");
			sender.sendMessage(ChatColor.DARK_GRAY + "Help: " + ChatColor.WHITE + "show this page");

		}

		return true;
	}

}
