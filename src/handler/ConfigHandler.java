package handler;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import mode.ModeType;
import reflex.GameManager;
import reflex.Reflex;

public class ConfigHandler {

	private static int numSpawns = 0;

	public static void initialize() {

		while (getConfig().contains("spawns." + (numSpawns + 1))) {

			numSpawns++;

		}

	}

	public static void resetConfig() {

		Reflex.instance.saveDefaultConfig();

	}

	public static void setAuto(boolean auto) {

		getConfig().set("auto", auto);

		saveConfig();

	}

	public static boolean getAuto() {

		return getConfig().getBoolean("auto");

	}

	public static void setMode(ModeType mode) {

		getConfig().set("mode", mode.toString());

		saveConfig();

	}

	public static ModeType getMode() {

		return ModeType.valueOf((String) getConfig().get("mode"));

	}

	public static void setMaxLifes(int maxLifes) {

		getConfig().set("lifes", maxLifes);

		saveConfig();

	}

	public static int getMaxLifes() {

		return getConfig().getInt("lifes");

	}

	public static Location getSpawn(int id) {

		String[] configValue = getConfig().getString("spawns." + id).split(":");

		int x = Integer.parseInt(configValue[0]);
		int y = Integer.parseInt(configValue[1]);
		int z = Integer.parseInt(configValue[2]);

		return new Location(GameManager.world, x, y, z);

	}

	public static int getNumSpawns() {

		return numSpawns;

	}

	private static FileConfiguration getConfig() {

		return Reflex.instance.getConfig();

	}

	private static void saveConfig() {

		Reflex.instance.saveConfig();

	}

}
