package handler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import reflex.KillType;
import title.Title;

public class MessageHandler {

	// public static String prefix = ChatColor.DARK_GRAY + "[" +
	// ChatColor.DARK_GREEN + "Reflex" + ChatColor.DARK_GRAY
	// + "] " + ChatColor.GRAY;

	public static void sendKillMessage(Player killer, Player killed) {

		sendTitle(killer, "", ChatColor.GOLD + "+1 | killed " + killed.getName());

		Bukkit.broadcastMessage(ChatColor.GREEN + killed.getName() + ChatColor.GRAY + " was slain by " + ChatColor.GREEN
				+ killer.getName());

	}

	public static void sendShotMessage(Player killer, Player killed) {

		sendTitle(killer, "", ChatColor.GOLD + "+1 | shot " + killed.getName());

		Bukkit.broadcastMessage(ChatColor.GREEN + killed.getName() + ChatColor.GRAY + " was shot by " + ChatColor.GREEN
				+ killer.getName());

	}

	public static void sendMidAirMessage(Player killer, Player killed) {

		sendTitle(killer, "", ChatColor.GOLD + "+2 | mid-air shot " + killed.getName());

		Bukkit.broadcastMessage("" + ChatColor.GREEN + killed.getName() + ChatColor.GRAY + " was mid-air shot by "
				+ ChatColor.GREEN + killer.getName());

	}

	public static void send360Message(Player killer, Player killed) {

		sendTitle(killer, "", ChatColor.GOLD + "+3 | 360'ed " + killed.getName());

		Bukkit.broadcastMessage("" + ChatColor.GREEN + ChatColor.BOLD + killed.getName() + ChatColor.GRAY
				+ ChatColor.BOLD + " was 360'ed by " + ChatColor.GREEN + ChatColor.BOLD + killer.getName());

	}

	public static void sendKillChainMessage(Player killer, KillType type) {

		broadcastTitle("", ChatColor.GOLD + type.toString() + ChatColor.GRAY + " KILL!");

		Bukkit.broadcastMessage(ChatColor.GOLD + type.toString() + ChatColor.GRAY + " KILL!");

	}

	public static void sendEliminateMessage(Player eliminated) {

		broadcastTitle("", ChatColor.RED + eliminated.getName() + ChatColor.YELLOW + " got eliminated!");

		Bukkit.broadcastMessage(ChatColor.RED + eliminated.getName() + ChatColor.YELLOW + " got eliminated!");

	}

	public static void sendStartMessage() {

		broadcastTitle(ChatColor.GREEN + "Game has started!", "");

		Bukkit.broadcastMessage(ChatColor.GREEN + "Game has started!");

	}

	// Not very elegant
	public static void sendTimeLeftMessage(int timeLeft) {

		String suffix = ChatColor.RED + "1" + ChatColor.YELLOW + " minute!";

		if (timeLeft == 30)
			suffix = ChatColor.RED + "30" + ChatColor.YELLOW + " seconds!";

		Bukkit.broadcastMessage(ChatColor.YELLOW + "Elimination in " + ChatColor.RED + suffix);

	}

	public static void sendEliminationTimerMessage() {

		broadcastTitle("", ChatColor.YELLOW + "Player with the " + ChatColor.RED + "lowest score" + ChatColor.YELLOW
				+ " will get eliminated in " + ChatColor.RED + "2" + ChatColor.YELLOW + " minutes!");

		Bukkit.broadcastMessage(ChatColor.YELLOW + "Player with the " + ChatColor.RED + "lowest score"
				+ ChatColor.YELLOW + " will get eliminated in " + ChatColor.RED + "2" + ChatColor.YELLOW + " minutes!");

	}

	public static void sendChatMessage(Player p, String message) {

		Bukkit.broadcastMessage(ChatColor.DARK_GREEN + p.getName() + ": " + ChatColor.GRAY + message);

	}

	public static void broadcastEndMessage() {

		broadcastTitle(ChatColor.GREEN + "Game force ended!", "");

		Bukkit.broadcastMessage(ChatColor.GREEN + "Game force ended!");

	}

	public static void broadcastEndMessage(String winner) {

		broadcastTitle(ChatColor.GREEN + winner + " won!", "");

		Bukkit.broadcastMessage(ChatColor.GREEN + winner + " has won!");

	}

	public static void sendTitle(final Player p, String message, String subMessage) {

		Title title = new Title(message, subMessage, 10, 20, 10);
		title.setTimingsToTicks();
		title.send(p);

	}

	public static void broadcastTitle(String message, String subMessage) {

		for (Player p : Bukkit.getOnlinePlayers())
			sendTitle(p, message, subMessage);

	}

	public static KillType increaseType(KillType type) {
		if (type == null) {
			return KillType.DOUBLE;
		}
		switch (type) {
		case DOUBLE:
			return KillType.TRIPLE;
		case TRIPLE:
			return KillType.QUADRA;
		case QUADRA:
			return KillType.PENTA;
		case PENTA:
			return KillType.HEXA;
		case HEXA:
			return KillType.INSANE;
		case INSANE:
			return KillType.INSANE;
		default:
			return KillType.DOUBLE;
		}
	}

	public static void playSound(KillType type) {

		for (Player p : Bukkit.getOnlinePlayers()) {

			playSound(p, type);

		}

	}

	public static void playSound(Player p, KillType type) {

		Sound sound = Sound.ENDERDRAGON_GROWL;

		if (type.equals(KillType.INSANE)) {

			sound = Sound.PORTAL_TRAVEL;

		}

		p.playSound(p.getLocation(), sound, 1, 1);

	}

}
