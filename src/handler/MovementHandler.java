package handler;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

public class MovementHandler {

	private static HashMap<UUID, Integer> rightTrickshotProgress = new HashMap<UUID, Integer>();

	private static HashMap<UUID, Float> rightTrickshotMap = new HashMap<UUID, Float>();

	private static HashMap<UUID, Long> rightTrickshotComplete = new HashMap<UUID, Long>();

	// p.isOnGround() is deprecated
	@SuppressWarnings("deprecation")
	public static void check360(Player p) {

		if (p.isOnGround()) {

			rightTrickshotComplete.remove(p.getUniqueId());

			rightTrickshotProgress.remove(p.getUniqueId());

			rightTrickshotMap.remove(p.getUniqueId());

		}

		if (!rightTrickshotMap.containsKey(p.getUniqueId())) {

			rightTrickshotMap.put(p.getUniqueId(), p.getLocation().getYaw());
			return;

		}

		if (rightTrickshotMap.get(p.getUniqueId()) - p.getLocation().getYaw() < 10) {

			if (rightTrickshotMap.get(p.getUniqueId()) <= p.getLocation().getYaw()) {

				if (rightTrickshotProgress.containsKey(p.getUniqueId())) {

					rightTrickshotProgress.put(p.getUniqueId(), rightTrickshotProgress.get(p.getUniqueId())
							+ Math.round((p.getLocation().getYaw() - rightTrickshotMap.get(p.getUniqueId()))));

				} else {

					rightTrickshotProgress.put(p.getUniqueId(),
							Math.round((p.getLocation().getYaw() - rightTrickshotMap.get(p.getUniqueId()))));

				}

				rightTrickshotMap.put(p.getUniqueId(), p.getLocation().getYaw());

			} else {

				if (rightTrickshotMap.containsKey(p.getUniqueId()))
					rightTrickshotMap.remove(p.getUniqueId());

				if (rightTrickshotProgress.containsKey(p.getUniqueId()))
					rightTrickshotProgress.remove(p.getUniqueId());

			}

		}

		if (rightTrickshotProgress.containsKey(p.getUniqueId()) && rightTrickshotProgress.get(p.getUniqueId()) >= 300) {

			rightTrickshotProgress.remove(p.getUniqueId());

			rightTrickshotMap.remove(p.getUniqueId());

			rightTrickshotComplete.put(p.getUniqueId(), System.currentTimeMillis());

			return;

		}
		/*
		 * Left side round too: else if (rightTrickshotMap.get(p.getUniqueId())
		 * - p.getLocation().getYaw() > 200) { if
		 * (rightTrickshotProgress.containsKey(p.getUniqueId()))
		 * rightTrickshotProgress.put(p.getUniqueId(),
		 * rightTrickshotProgress.get(p.getUniqueId()) + 40); else
		 * rightTrickshotProgress.put(p.getUniqueId(), 40); }
		 */

	}

	public static boolean hasCompleted360(Player p) {

		return rightTrickshotComplete.containsKey(p.getUniqueId());

	}

}
