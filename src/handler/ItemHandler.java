package handler;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ItemHandler {

	public static ItemStack sword = new ItemStack(Material.WOOD_SWORD);
	public static ItemStack bow = new ItemStack(Material.BOW);
	public static ItemStack arrow = new ItemStack(Material.ARROW, 1);

	public static void initialize() {

		sword.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
		bow.addUnsafeEnchantment(Enchantment.DURABILITY, 10);
		arrow.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 10);

	}

	public static void clear() {

		for (Player p : Bukkit.getOnlinePlayers())
			p.getInventory().clear();

	}

	public static void applyKit() {

		for (Player p : Bukkit.getOnlinePlayers())
			applyKit(p);

	}

	public static void applyKit(Player p) {

		p.getInventory().clear();
		p.getInventory().addItem(sword, bow, arrow);

	}

	public static void addArrow(Player p, int amount) {

		ItemStack arrowStack = arrow.clone();
		arrowStack.setAmount(amount);

		p.getInventory().addItem(arrowStack);

	}

}
