package reflex;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import handler.CommandHandler;
import handler.ConfigHandler;
import handler.ItemHandler;
import listener.GameListener;
import listener.OverrideListener;
import util.FileUtil;

public class Reflex extends JavaPlugin {

	public String deathMes;

	public static Reflex instance;

	public void onEnable() {

		registerListeners();
		registerInstance();

		ConfigHandler.initialize();

		ItemHandler.initialize();

		GameManager.initialize();

	}

	public void onDisable() {

		GameManager.deInitialize();

		World world = GameManager.world;

		Bukkit.unloadWorld(world, false);
		FileUtil.purgeWorldFiles(world);

	}

	public void registerListeners() {

		PluginManager pm = getServer().getPluginManager();

		pm.registerEvents(new GameListener(), this);
		pm.registerEvents(new OverrideListener(), this);

	}

	public void registerInstance() {

		instance = this;

	}

	// Subject to change, but for now it works
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

		return CommandHandler.onCommand(sender, command, label, args);

	}

}
