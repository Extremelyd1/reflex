package reflex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;

import handler.ConfigHandler;
import handler.ItemHandler;
import handler.MessageHandler;
import handler.ScoreboardHandler;
import mode.Elimination;
import mode.Lives;
import mode.Mode;
import mode.ModeType;
import mode.Normal;

public class GameManager {

	public static Mode mode;
	public static ModeType modeType;

	public static GameState state = GameState.PRE_GAME;

	public static boolean auto;

	public static int maxLives;

	public static World world;

	public static Random random;

	public static ScoreboardHandler scoreboardHandler;

	public static HashMap<UUID, Long> lastKill = new HashMap<UUID, Long>();
	public static HashMap<UUID, KillType> killChain = new HashMap<UUID, KillType>();

	private static ArrayList<Location> spawns = new ArrayList<Location>();

	public static void initialize() {

		// To avoid getting the world wrong
		world = Bukkit.getWorlds().get(0);

		auto = ConfigHandler.getAuto();

		changeMode(ConfigHandler.getMode());

		maxLives = ConfigHandler.getMaxLifes();

		random = new Random();

		for (int i = 1; i <= ConfigHandler.getNumSpawns(); i++) {

			spawns.add(ConfigHandler.getSpawn(i));

		}

	}

	public static void deInitialize() {

		ConfigHandler.setAuto(auto);

		ConfigHandler.setMode(modeType);

		ConfigHandler.setMaxLifes(maxLives);

	}

	public static void changeMode(ModeType newMode) {

		modeType = newMode;

		switch (newMode) {

		case NORMAL:
			mode = new Normal();
			break;

		case ELIMINATION:
			mode = new Elimination();
			break;

		case LIVES:
			mode = new Lives();
			break;

		default:
			break;

		}

		scoreboardHandler = new ScoreboardHandler(modeType);

	}

	public static void start() {

		GameManager.state = GameState.IN_GAME;

		mode.start();

	}

	public static void end() {

		GameManager.state = GameState.POST_GAME;

		mode.end();

	}

	public static void end(String winner) {

		GameManager.state = GameState.POST_GAME;

		mode.end(winner);

	}

	public static void registerKill(Player killer, Player killed, int points) {

		if (lastKill.containsKey(killer.getUniqueId())) {

			if (System.currentTimeMillis() - lastKill.get(killer.getUniqueId()) < 2000) {

				if (!killChain.containsKey(killer.getUniqueId())) {

					killChain.put(killer.getUniqueId(), KillType.DOUBLE);

				} else {

					KillType newType = KillType.increaseKillType(killChain.get(killer.getUniqueId()));

					killChain.put(killer.getUniqueId(), newType);

				}

				MessageHandler.sendKillChainMessage(killer, killChain.get(killer.getUniqueId()));

				if (killChain.get(killer.getUniqueId()).equals(KillType.INSANE))
					for (Player p : Bukkit.getOnlinePlayers())
						p.playSound(p.getLocation(), Sound.ENDERDRAGON_GROWL, 6, 1);

			} else {

				killChain.remove(killer.getUniqueId());

			}

		}

		lastKill.put(killer.getUniqueId(), System.currentTimeMillis());

		if (modeType.equals(ModeType.NORMAL)) {

			Normal normalMode = (Normal) mode;

			int currentScore = normalMode.scores.get(killer.getUniqueId());

			normalMode.scores.put(killer.getUniqueId(), currentScore + points);

			scoreboardHandler.setPoints(killer.getName(), currentScore + points, true);

			if (currentScore + points == Bukkit.getOnlinePlayers().size() * 10) {

				end(killer.getName());

				return;

			}

		} else if (modeType.equals(ModeType.ELIMINATION)) {

			Elimination elimMode = (Elimination) mode;

			int currentScore = elimMode.scores.get(killer.getUniqueId());

			elimMode.scores.put(killer.getUniqueId(), currentScore + points);

			scoreboardHandler.setPoints(killer.getName(), currentScore + points, true);

		} else if (modeType.equals(ModeType.LIVES)) {

			Lives lifeMode = (Lives) mode;

			int currentLives = lifeMode.lifes.get(killed.getUniqueId());

			if (currentLives == 1) {

				lifeMode.alive.remove(killed.getUniqueId());

				MessageHandler.sendEliminateMessage(killed);

				scoreboardHandler.removePlayer(killed);

				if (lifeMode.alive.size() == 1) {

					end(Bukkit.getPlayer(lifeMode.alive.get(0)).getName());

					return;

				}

			} else {

				lifeMode.lifes.put(killed.getUniqueId(), currentLives - 1);

				scoreboardHandler.setPoints(killed.getName(), currentLives - 1, false);

			}

		}

		if (!killer.isDead())
			killer.setHealth(20);

		ItemHandler.addArrow(killer, points);

	}

	public static void resetKillChain(UUID uuid) {

		lastKill.remove(uuid);
		killChain.remove(uuid);

	}

	public static Location getRandomSpawn() {

		int randomInt = random.nextInt(spawns.size());

		return spawns.get(randomInt);

	}

}
