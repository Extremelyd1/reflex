package reflex;

public enum KillType {

	DOUBLE, TRIPLE, QUADRA, PENTA, HEXA, INSANE;

	public static KillType increaseKillType(KillType type) {

		switch (type) {

		default:
			return DOUBLE;
		case DOUBLE:
			return TRIPLE;
		case TRIPLE:
			return QUADRA;
		case QUADRA:
			return PENTA;
		case PENTA:
			return HEXA;
		case HEXA:
			return INSANE;
		case INSANE:
			return INSANE;

		}

	}

}
