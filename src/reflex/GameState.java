package reflex;

public enum GameState {

	PRE_GAME, STARTING, IN_GAME, POST_GAME, STOPPING;

}
