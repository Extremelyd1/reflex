package listener;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.metadata.FixedMetadataValue;

import handler.ItemHandler;
import handler.MessageHandler;
import handler.MovementHandler;
import handler.SpectatorHandler;
import mode.Elimination;
import mode.Lives;
import mode.ModeType;
import reflex.GameManager;
import reflex.GameState;
import reflex.Reflex;
import util.FireworkUtil;

public class GameListener implements Listener {

	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {

		Player p = e.getPlayer();

		if (GameManager.modeType.equals(ModeType.LIVES)) {

			Lives lifeMode = (Lives) GameManager.mode;

			if (lifeMode.alive.contains(p.getUniqueId())) {

				int lifesLeft = lifeMode.lifes.get(p.getUniqueId());

				MessageHandler.sendTitle(p, ChatColor.GREEN + "Game has started!",
						ChatColor.YELLOW + "You have " + ChatColor.RED + lifesLeft + ChatColor.YELLOW + " life"
								+ (lifesLeft == 1 ? "" : "s") + " remaining!");

				ItemHandler.applyKit(p);

			} else {

				for (UUID alivePlayerUUID : lifeMode.alive) {

					Bukkit.getPlayer(alivePlayerUUID).hidePlayer(p);

				}

				SpectatorHandler.setInteractable(p, false);

			}

		} else if (GameManager.modeType.equals(ModeType.ELIMINATION)) {

			Elimination elimMode = (Elimination) GameManager.mode;

			if (elimMode.alive.contains(p.getUniqueId())) {

				ItemHandler.applyKit(p);

			} else {

				for (UUID alivePlayerUUID : elimMode.alive) {

					Bukkit.getPlayer(alivePlayerUUID).hidePlayer(p);

				}

				SpectatorHandler.setInteractable(p, false);

			}

		} else {

			if (GameManager.state.equals(GameState.IN_GAME))
				ItemHandler.applyKit(p);

		}

		// Override if game ended during respawning of player
		if (!GameManager.state.equals(GameState.IN_GAME)) {

			p.getInventory().clear();

			SpectatorHandler.setInteractable(p, false);

		}

		p.setArrowsStuck(0);

		e.setRespawnLocation(GameManager.getRandomSpawn());

	}

	// Only sword kills are accounted for here
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {

		Player p = e.getEntity();

		e.getDrops().clear();

		// Not a sword kill
		if (p.getKiller() == null) {

			e.setDeathMessage(null);
			return;

		}

		if (e.getDeathMessage().contains("was slain by")) {

			Player killer = e.getEntity().getKiller();

			MessageHandler.sendKillMessage(killer, p);

			GameManager.registerKill(killer, p, 1);

		}

		GameManager.resetKillChain(p.getUniqueId());

		e.setDeathMessage(null);

	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDamageByEntity(EntityDamageByEntityEvent e) {

		if (!(e.getEntity() instanceof Player))
			return;

		Player damaged = (Player) e.getEntity();

		if (damaged.isDead())
			return;

		if (e.getDamager() instanceof Player) {

			Player damager = (Player) e.getDamager();

			if (damager.isDead()) {
				
				e.setCancelled(true);
				return;
				
			}

			if (damager.getItemInHand().getType().equals(Material.WOOD_SWORD)) {

				// Replenish durability
				damager.getItemInHand().setDurability((short) 0);

			}

			if (!GameManager.state.equals(GameState.IN_GAME)) {

				e.setCancelled(true);
				return;

			}

			if (GameManager.modeType.equals(ModeType.LIVES)) {

				Lives lifeMode = (Lives) GameManager.mode;

				if (!lifeMode.alive.contains(damager.getUniqueId())) {

					e.setCancelled(true);
					return;

				}

			} else if (GameManager.modeType.equals(ModeType.ELIMINATION)) {

				Elimination elimMode = (Elimination) GameManager.mode;

				if (!elimMode.alive.contains(damager.getUniqueId())) {

					e.setCancelled(true);
					return;

				}

			}

		} else if (e.getCause().equals(DamageCause.PROJECTILE)) {

			Projectile arrow = (Projectile) e.getDamager();
			Player shooter = (Player) arrow.getShooter();

			// You cannot shoot yourself
			if (damaged.equals(shooter)) {

				e.setCancelled(true);
				return;

			}

			// Play sound on location
			for (Player listener : Bukkit.getOnlinePlayers())
				listener.playSound(damaged.getLocation(), Sound.EXPLODE, 3, 1);

			// Make firework explosion
			FireworkEffect fe = FireworkEffect.builder().flicker(false).withColor(Color.RED).withFade(Color.BLACK)
					.trail(true).build();

			// Different firework explosion for 360's
			if (arrow.hasMetadata("360")) {

				fe = FireworkEffect.builder().with(Type.BALL_LARGE).flicker(false).withColor(Color.YELLOW)
						.withFade(Color.BLACK).trail(true).build();

				MessageHandler.send360Message(shooter, damaged);

				GameManager.registerKill(shooter, damaged, 3);

				// Deprecated, should find better implementation
			} else if (!damaged.isOnGround() || !shooter.isOnGround()) {

				MessageHandler.sendMidAirMessage(shooter, damaged);

				GameManager.registerKill(shooter, damaged, 2);

			} else {

				MessageHandler.sendShotMessage(shooter, damaged);

				GameManager.registerKill(shooter, damaged, 1);

			}

			// Play firework twice for effect
			FireworkUtil.playFirework(damaged.getWorld(), damaged.getLocation(), fe);
			FireworkUtil.playFirework(damaged.getWorld(), damaged.getLocation(), fe);

			// Insta-kill bow, maybe overkill
			damaged.damage(200);

			if (!shooter.isDead())
				shooter.setHealth(20);

			// I don't know how this could happen, but just in case
		} else {

			e.setCancelled(true);

		}

	}

	@EventHandler
	public void onLaunch(ProjectileLaunchEvent e) {

		// Skeleton firing
		if (!(e.getEntity().getShooter() instanceof Player)) {

			e.setCancelled(true);
			return;

		}

		Player shooter = (Player) e.getEntity().getShooter();

		// Replenish durability
		shooter.getItemInHand().setDurability((short) 0);

		if (MovementHandler.hasCompleted360(shooter)) {

			e.getEntity().setMetadata("360", new FixedMetadataValue(Reflex.instance, ""));

		}

	}

	@EventHandler
	public void onFall(EntityDamageEvent e) {

		if (!GameManager.state.equals(GameState.IN_GAME)) {

			e.setCancelled(true);

		} else if (e.getCause().equals(DamageCause.FALL)) {

			e.setCancelled(true);

		}

	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {

		MovementHandler.check360(e.getPlayer());

	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {

		Player p = e.getPlayer();

		e.setQuitMessage(ChatColor.GREEN + p.getName() + ChatColor.GRAY + " has left the game!");

		if (GameManager.state.equals(GameState.IN_GAME)) {

			GameManager.scoreboardHandler.removePlayer(p);

			if (GameManager.mode.equals(ModeType.LIVES)) {

				Lives lifeMode = (Lives) GameManager.mode;

				lifeMode.alive.remove(p.getUniqueId());

				if (lifeMode.alive.size() == 1) {

					GameManager.end(Bukkit.getPlayer(lifeMode.alive.get(0)).getName());

				}

			} else if (GameManager.mode.equals(ModeType.ELIMINATION)) {

				Elimination elimMode = (Elimination) GameManager.mode;

				elimMode.alive.remove(p.getUniqueId());

				if (elimMode.alive.size() == 1) {

					GameManager.end(Bukkit.getPlayer(elimMode.alive.get(0)).getName());

				}

			}

		}

		if (Bukkit.getOnlinePlayers().size() < 2 && GameManager.auto) {

			Bukkit.getScheduler().scheduleSyncDelayedTask(Reflex.instance, new Runnable() {

				public void run() {

					for (Player p : Bukkit.getOnlinePlayers())
						p.kickPlayer(ChatColor.GREEN + "Thanks for playing!");

					Bukkit.shutdown();

				}

			}, 200L);

		}

	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {

		Player p = e.getPlayer();

		e.setJoinMessage(ChatColor.GREEN + p.getName() + ChatColor.GRAY + " has joined the game!");

		if (GameManager.state.equals(GameState.IN_GAME)) {

			if (GameManager.mode.equals(ModeType.LIVES)) {

				Lives lifeMode = (Lives) GameManager.mode;

				for (UUID alivePlayerUUID : lifeMode.alive) {

					Bukkit.getPlayer(alivePlayerUUID).hidePlayer(p);

				}

				SpectatorHandler.setInteractable(p, false);

			} else if (GameManager.mode.equals(ModeType.ELIMINATION)) {

				Elimination elimMode = (Elimination) GameManager.mode;

				for (UUID alivePlayerUUID : elimMode.alive) {

					Bukkit.getPlayer(alivePlayerUUID).hidePlayer(p);

				}

				SpectatorHandler.setInteractable(p, false);

			} else {

				GameManager.scoreboardHandler.addPlayer(p);

				p.teleport(GameManager.getRandomSpawn());

				p.playSound(p.getLocation(), Sound.ENDERDRAGON_HIT, 20, 1);

				p.setArrowsStuck(0);

				ItemHandler.applyKit();

			}

		} else if (GameManager.state.equals(GameState.PRE_GAME)) {

			p.teleport(GameManager.getRandomSpawn());

			p.getInventory().clear();

			SpectatorHandler.setInteractable(p, false);

			if (Bukkit.getOnlinePlayers().size() == 2 && GameManager.auto) {

				GameManager.state = GameState.STARTING;

				Bukkit.getScheduler().scheduleSyncDelayedTask(Reflex.instance, new Runnable() {

					public void run() {

						GameManager.start();

					}

				}, 40L);

			}

		} else {

			p.teleport(GameManager.getRandomSpawn());

			p.getInventory().clear();

			SpectatorHandler.setInteractable(p, false);

		}

	}

	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e) {

		// Prevent players from hiding in lives mode
		if (GameManager.mode.equals(ModeType.LIVES) && GameManager.state.equals(GameState.IN_GAME) && e.isSneaking())
			e.getPlayer().setSneaking(false);

	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {

		e.setCancelled(true);

		MessageHandler.sendChatMessage(e.getPlayer(), e.getMessage());

	}

	@EventHandler
	public void onMotd(ServerListPingEvent e) {

		String suffix = "Pre-game";

		switch (GameManager.state) {

		case STARTING:
			suffix = "Starting";
			break;

		case IN_GAME:
			suffix = "In-game";
			break;

		case POST_GAME:
			suffix = "Post-game";
			break;

		default:
			break;

		}

		e.setMotd(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_GREEN + "Reflex" + ChatColor.DARK_GRAY + "] "
				+ ChatColor.GRAY + suffix);

	}

}
